#include <Wire.h>
#include <stdio.h>
#include <stdlib.h>
#include "BasicStepperDriver.h"
#include "MPU6050_6Axis_MotionApps20.h"
#include <Regexp.h>
#include "DHT.h"
#include "I2Cdev.h"

#define enableMotor true
#define MOTOR_STEPS 200
#define MICROSTEPS 1
#define RPM 120 * 2 //szybkosc

#define DIR 12 //14
#define Enable 13 //15
#define STEP 11 //16

float fullCycle = 360 * 5.18; //360*5.18 = jeden pelen obrot silnika pololu

int pinEngine1a = 4; // d10
int pinEngine1b = A0; // d9 .   /// kreci
int pinEngine1s = 10; // d6 pwm

int pinEngine2a = 5; // d10
int pinEngine2b = 6; // d9 .   /// kreci
int pinEngine2s = 10; // d6 pwm

BasicStepperDriver stepper(MOTOR_STEPS, DIR, STEP);

uint16_t packetSize = 0;
MPU6050 accelerometer(0x68);

#define DHTPIN 8
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Wire.begin(7);
  Serial.begin(38400);
  while (!Serial);

  initGyro(accelerometer, packetSize);

  dht.begin();
  pinMode(Enable, OUTPUT);
  stepper.begin(RPM, MICROSTEPS);
  digitalWrite(Enable, HIGH);

  Wire.onReceive(receiveEvent);
}

void receiveEvent(int howMany) {
  while (1 < Wire.available()) { // loop through all but the last
    char c = Wire.read(); // receive byte as a character
    Serial.print(c);         // print the character
  }
  int x = Wire.read();    // receive byte as an integer
  Serial.println(x);         // print the integer
}

void loop() {

  unsigned wait_time_micros = stepper.nextAction();
  if (wait_time_micros <= 0) {
    stepper.startBrake();
  }

  if (Serial.available() > 0) {
    char lastline[50];
    int number = readSerial(lastline);

    if (strcmp(lastline, ">") == 0) {
      engine1(number, LOW, HIGH);
      engine2(number, LOW, HIGH);
    }
    if (strcmp(lastline, "<") == 0) {
      engine1(number, HIGH, LOW);
      engine2(number, HIGH, LOW);
    }

    if (strcmp(lastline, "z") == 0) {
      Serial.print(F("disable!\n"));
      digitalWrite(Enable, HIGH);
    }

    if (strcmp(lastline, "x") == 0) {
      Serial.print(F("enable!\n"));
      digitalWrite(Enable, LOW);
      stepper.startBrake();
    }

    if (strcmp(lastline, "c") == 0) {
      Serial.print(F("360!\n"));
      stepper.disable();
      stepper.begin(RPM, MICROSTEPS);
      stepper.startMove(fullCycle * number);
      //stepper.rotate(fullCycle * 1);
    }

    if (strcmp(lastline, "v") == 0) {
      Serial.print(F("360!\n"));
      stepper.disable();
      stepper.begin(RPM / 3, MICROSTEPS);
      stepper.startMove(fullCycle * number);
      //stepper.rotate(fullCycle * 1);
    }

    if (strcmp(lastline, "p") == 0) {
      float h = dht.readHumidity();
      float t = dht.readTemperature();
      float hic = dht.computeHeatIndex(t, h, false);

      Serial.print("Humidity: ");
      Serial.print(h);
      Serial.print(" %\t");
      Serial.print("Temperature: ");
      Serial.print(t);
      Serial.print(" *C ");
      Serial.print("Heat index: ");
      Serial.print(hic);
      Serial.print(" *C \n");
    }

    if (strcmp(lastline, "g") == 0) {
      Serial.println(F("g!"));
      getGyro("GYRO1#", accelerometer);
    }

    if (strcmp(lastline, "b") == 0) {
      stepper.startBrake();
    }

    String msg = String(number) + String(",") + String(lastline);
    Serial.println(msg);
    wireWrite(10, msg);
  }

  delay(5);
}
