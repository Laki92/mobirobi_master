boolean isNumeric(String str) {
  unsigned int stringLength = str.length();

  if (stringLength == 0) {
    return false;
  }

  boolean seenDecimal = false;

  for (unsigned int i = 0; i < stringLength; ++i) {
    if (isDigit(str.charAt(i))) {
      continue;
    }

    if (str.charAt(i) == '.') {
      if (seenDecimal) {
        return false;
      }
      seenDecimal = true;
      continue;
    }
    return false;
  }
  return true;
}


#ifdef MPU6050_ADDRESS_AD0_LOW
void initGyro(MPU6050 &accelerometer, uint16_t &packetSize) {

  bool dmpReady = false;
  uint8_t devStatus = accelerometer.dmpInitialize();
  uint8_t mpuIntStatus;
  accelerometer.initialize();

  if (devStatus == 0) {
    Serial.println(F("Enabling DMP..."));
    accelerometer.setDMPEnabled(true);
    Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));

    mpuIntStatus = accelerometer.getIntStatus();
    Serial.println(F("DMP ready! Waiting for first interrupt..."));
    dmpReady = true;
    packetSize = accelerometer.dmpGetFIFOPacketSize();
  } else {
    Serial.print(F("DMP Initialization failed (code "));
    Serial.print(devStatus);
    Serial.println(F(")"));
  }

  accelerometer.setXGyroOffset(220);
  accelerometer.setYGyroOffset(76);
  accelerometer.setZGyroOffset(-85);
  accelerometer.setZAccelOffset(1788);
}

String getGyro(String TAG,  MPU6050 mpu) {
  uint8_t mpuIntStatus = mpu.getIntStatus();
  uint16_t fifoCount = mpu.getFIFOCount();
  if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
    mpu.resetFIFO(); //overflow
    return getGyro(TAG,  mpu);
  } else {
    float euler[3];
    uint8_t fifoBuffer[64];

    Quaternion q;
    while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
    mpu.getFIFOBytes(fifoBuffer, packetSize);
    fifoCount -= packetSize;

    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetEuler(euler, &q);
    float x= euler[0] * 180 / M_PI;
    float y= euler[1] * 180 / M_PI;
    float z= euler[2] * 180 / M_PI;
    String S = "|";
    String msg = TAG + S + x + S + y + S + z;
    Serial.println(msg);
    return msg;
  }
}
#endif

int readWire(char lastline[50]) {
  int k = 0;
  int number = 0;
  while (Wire.available() > 0) {
    char readed = Wire.read();
    String toTest = String(readed);
    if (isNumeric(toTest) == true) {
      int last = toTest.toInt();
      number = number * 10 + last;
      continue;
    }
    if (toTest.equals(",")) {
      continue;
    }

    lastline[k] = readed;
    k++;
  }
  lastline[k] = '\0';
  return number;
}

int readSerial(char lastline[50]) {
  int k = 0;
  int number = 0;
  while (Serial.available() > 0) {
    char readed = Serial.read();
    String toTest = String(readed);
    if (isNumeric(toTest) == true) {
      int last = toTest.toInt();
      number = number * 10 + last;
      continue;
    }
    if (toTest.equals(",")) {
      continue;
    }

    lastline[k] = readed;
    k++;
  }
  lastline[k] = '\0';
  return number;
}

void engine1(uint8_t s, uint8_t a, uint8_t b) {
#if defined(enableMotor)
  analogWrite(pinEngine1s, s);
  digitalWrite(pinEngine1a, a);
  digitalWrite(pinEngine1b, b);
#endif
}

void engine2(uint8_t s, uint8_t a, uint8_t b) {
#if defined(enableMotor)
  analogWrite(pinEngine2s, s);
  digitalWrite(pinEngine2a, a);
  digitalWrite(pinEngine2b, b);
#endif
}

void serwo1(uint8_t s, uint8_t a, uint8_t b) {
#if defined(enableSerwo)
  analogWrite(pinMotor1s, s);
  digitalWrite(pinMotor1a, a);
  digitalWrite(pinMotor1b, b);
#endif
}

void serwo2(uint8_t s, uint8_t a, uint8_t b) {
#if defined(enableSerwo)
  analogWrite(pinMotor2s, s);
  digitalWrite(pinMotor2a, a);
  digitalWrite(pinMotor2b, b);
#endif
}

void wireWrite(int port, String lastline) {
  char charBuf[50];
  lastline.toCharArray(charBuf, 50);
  wireWrite(port, charBuf);
}

void wireWrite(int port, char lastline[50]) {
  Wire.beginTransmission(port); // transmit to device #8
  Wire.write(lastline);        // sends five bytes
  Wire.endTransmission();    // stop transmitting
}
